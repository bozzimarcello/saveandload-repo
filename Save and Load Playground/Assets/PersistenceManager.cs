﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

public class PersistenceManager : MonoBehaviour
{
    // single manager instance reference
    private static PersistenceManager persistenceManager;

    private PlayerController playerController;
    private string dataFileName = "/GameStatus.dat";
    private string dataFilePath;

    // identificazione della mia crittografia
    // chiave arbitraria di 32 byte
    private byte[] myKey = {
                                1,2,3,4,5,6,7,8,
                                1,2,3,4,5,6,7,8,
                                1,2,3,4,5,6,7,8,
                                1,2,3,4,5,6,7,8
                            };
    // vettore arbitraria di inizializzazione di 16 byte
    private byte[] myIV = {
                                1,2,3,4,5,6,7,8,
                                1,2,3,4,5,6,7,8
                            };

    void Awake()
    {
        // implementiamo il pattern Singleton per 
        // avere persistenza tra diverse Scene
        if (persistenceManager == null)
        { 
            DontDestroyOnLoad(gameObject);
            persistenceManager = this;
            playerController = GameObject.FindObjectOfType<PlayerController>();
            dataFilePath = Application.persistentDataPath + dataFileName;
            Debug.Log("dataFilePath: " + dataFilePath);
        }
        else if (persistenceManager != this) 
        {
            Destroy(gameObject);
        }

        Debug.Log(gameObject.name);

        Load();
    }

    public void Next()
    {
        Save();
    }

    public void Save()
    {
        Debug.Log("Saving...");

        try
        { 
            using (FileStream myStream = new FileStream(dataFilePath, FileMode.OpenOrCreate, FileAccess.Write))
            {
                using (Aes myAes = Aes.Create())
                {
                    ICryptoTransform encryptor = myAes.CreateEncryptor(myKey, myIV);

                    using (Stream cryptoStream = new CryptoStream(myStream, encryptor, CryptoStreamMode.Write))
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        formatter.Serialize(cryptoStream, playerController.gameStatus);
                    }
                }
            }
        }
        catch (System.Exception e)
        {
            Debug.LogError("Runtime error: " + e.Message);
        }

    }
    public void Load()
    {
        Debug.Log("Loading...");
        try
        {
            if (File.Exists(dataFilePath))
            {
                using (FileStream myStream = new FileStream(dataFilePath, FileMode.Open, FileAccess.Read))
                {
                    using (Aes myAes = Aes.Create())
                    {
                        ICryptoTransform decriptor = myAes.CreateDecryptor(myKey, myIV);

                        using (Stream cryptoStream = new CryptoStream(myStream, decriptor, CryptoStreamMode.Read))
                        {
                            BinaryFormatter formatter = new BinaryFormatter();
                            playerController.gameStatus = formatter.Deserialize(cryptoStream) as GameStatus;
                        }
                    }
                }
            }
        }
        catch (System.Exception e)
        {
            Debug.LogError("Runtime error: " + e.Message);
        }
    }
}
