﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SerializableVector3
{
    public float x;
    public float y;
    public float z;

    public Vector3 GetVector3()
    {
        return new Vector3(x, y, z);
    }

    public void SetVector3(Vector3 vector)
    {
        x = vector.x;
        y = vector.y;
        z = vector.z;
    }
}

[System.Serializable]
public class GameStatus
{
    public string PlayerName;
    public float PlayerHealth;
    public int PlayerGold;
    public SerializableVector3 position;
}
