﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text playerName;
    public Text playerHealth;
    public Text playerGold;
    private PlayerController player;

    public void Start()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
    }

    public void Update()
    {
        MapValuesFromPlayer();
    }

    public void RandomizeStats()
    {
        playerName.text = "Frodo";
        playerHealth.text = UnityEngine.Random.Range(10, 1000).ToString();
        playerGold.text = UnityEngine.Random.Range(500, 20000).ToString();

        MapValuesToPlayer();
    }

    private void MapValuesToPlayer()
    {
        player.gameStatus.PlayerName = playerName.text;
        player.gameStatus.PlayerHealth = float.Parse(playerHealth.text);
        player.gameStatus.PlayerGold = int.Parse(playerGold.text);
    }

    public void MapValuesFromPlayer()
    {
        playerName.text = player.gameStatus.PlayerName;
        playerHealth.text = player.gameStatus.PlayerHealth.ToString();
        playerGold.text = player.gameStatus.PlayerGold.ToString();
    }
}
